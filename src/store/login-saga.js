import { takeEvery, put, call } from 'redux-saga/effects'
import LoginService from '../api/user-api'

export default function* loginSaga() {
  yield takeEvery('LOGIN_SAGA', loginHandle)
}

function* loginHandle(action) {
  try {
    const res1 = yield call(LoginService.login, action.payload);
    const res2 = yield call(LoginService.getMoreUserInfo, res1);

    yield put({type: 'LOGIN_SUCCESS', payload: res2});
  } catch(err) {
    yield put({type: 'LOGIN_FAILURE', payload: err});
  }
}