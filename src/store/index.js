import { createStore, combineReducers, applyMiddleware } from 'redux';
import { loginReducer } from './login-reducer';
import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import loginSaga from './login-saga';

const sageMiddleware = createSagaMiddleware();

const store = createStore(
  combineReducers({user: loginReducer}), 
  applyMiddleware(sageMiddleware)
);

sageMiddleware.run((loginSaga));

export default store;

