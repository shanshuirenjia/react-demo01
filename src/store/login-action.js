// export const login = (userInfo) => {
//   console.log(userInfo);
//   return {type: 'LOGIN_SUCCESS', userInfo}
// };

import LoginService from "../api/user-api";

// 异步更新数据, 方式一, 回调
// export const login = (userInfo) => {
//   return (dispatch) => loginPromise(dispatch, userInfo);
// }

// const loginPromise = (dispatch, userInfo) => {
//   LoginService.login(userInfo).then((res) => {
//     dispatch({type: 'LOGIN_SUCCESS', res});
//   }, (err) => {
//     dispatch({type: 'LOGIN_FAILURE', err});
//   });
// }

// export const login = (userInfo) => (dispatch) => {
//    return LoginService.login(userInfo).then((res) => {
//      LoginService.getMoreUserInfo(res).then(res => {
//        dispatch({type: 'LOGIN_SUCCESS', payload: res});
//      }, (err) => {
//        dispatch({type: 'LOGIN_FAILURE', payload: err});
//      })
//   }, (err) => {
//     dispatch({type: 'LOGIN_FAILURE', payload: err});
//   });
// }

// 异步更新数据, 方式二, sync/await

// export const login = (userInfo) => async (dispatch) => {
//   try {
//     let res = await LoginService.login(userInfo);
//     if (res) {
//       res = await LoginService.getMoreUserInfo(res);
//       dispatch({type: 'LOGIN_SUCCESS', payload: res});
//     }
//   } catch(err) {
//     dispatch({type: 'LOGIN_SUCCESS', payload: err});
//   };
// }

// 异步更新数据, 方式三, saga

export const login = (userInfo) => ({type: 'LOGIN_SAGA', payload: userInfo});