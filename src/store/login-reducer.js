const userInit = {
  isLogin: false,
  userInfo: {id: null, name: "", score: 0},
  loading: false,
  err: {msg: ""}
};

export const loginReducer = (state = {...userInit}, {type, payload}) => {
  switch(type) {
    case 'LOGIN_SUCCESS':
      return {...state, isLogin: true, isLoading: true, userInfo: {...payload}};
    case 'LOGIN_FAILURE':
      return {...state, ...userInit, ...payload};
    default:
      return state;
  }
};