import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect, Route } from 'react-router';

const propTypes = {

};

class PrivateRoute extends React.Component {

  render() {
    const { user, location, component: Component, ...resetProps } = this.props;
    if (!user.isLogin) {
      return <Redirect to={{pathname: '/login', state: {from: location.pathname}}} />
    }
    return (
      <Route {...resetProps} render={() => (<Component {...this.props} />)} />
    );
  }
}

PrivateRoute.propTypes = propTypes;

export default connect(
  ({user}) => ({user})
)(PrivateRoute);
