import React from 'react';
import { 
  BrowserRouter as Router,
  Link,
  Route,
  Switch,
} from 'react-router-dom';
import PrivateRoute from './private-route';
import HomePage from '../pages/home-page';
import UserPage from '../pages/user-page';
import LoginPage from '../pages/login-page';
import _404Page from '../pages/_404-page';

export default function Routes(props) {

  return (
    <Router>
      <Link to="/">首页</Link>
      <Link to="/login">登陆</Link>
      <Link to="/user">用户中心</Link>
      <Switch>
        <Route exact path="/" component={HomePage}></Route>
        <Route path="/login" component={LoginPage}></Route>
        <PrivateRoute path="/user" component={UserPage}></PrivateRoute>
        <Route component={_404Page}></Route>
      </Switch>
    </Router>
  );
}
