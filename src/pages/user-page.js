import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

const propTypes = {

};

class UserPage extends React.Component {

  render() {
    console.log(this.props);
    const { user, location } = this.props;
    if (!user.isLogin) {
      return <Redirect to={{pathname: '/login', state: {from: location.pathname}}} />;
    }
    const { userInfo } = user;
    console.log(userInfo);
    return (
      <div>
        <div>id: {userInfo.id}</div>
        <div>name: {userInfo.name}</div>
        <div>score: {userInfo.score}</div>
      </div>
    );
  }
}

UserPage.propTypes = propTypes;

export default connect(
  ({user}) => ({user})
)(UserPage);
