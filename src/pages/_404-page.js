import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {

};

class _404Page extends React.Component {

  render() {
    return (
      <div>404</div>
    );
  }
}

_404Page.propTypes = propTypes;

export default _404Page;
