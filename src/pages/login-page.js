import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { login } from '../store/login-action';


const propTypes = {

};

class LoginPage extends React.Component {

  componentDidMount() {
    setTimeout(() => {
      const { login } = this.props;
      login({name: '小明'});
    }, 3000);
  }

  render() {
    const { user, location } = this.props;
    if (user.isLogin) {
      const { from = '/' } = location.state || {};
      return <Redirect to={from} />
    }

    return (
      <div>LoginPage, loading......</div>
    );
  }
}

LoginPage.propTypes = propTypes;

export default connect(
  ({user}) => ({user}),
  {
    login
  }
)(LoginPage);
